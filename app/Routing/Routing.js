import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { Animated, Dimensions, Image, Platform, StyleSheet, Text, TouchableOpacity, View } from 'react-native';
import 'react-native-gesture-handler';
import { NavigationContainer } from '@react-navigation/native';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Home from '../Wallpaper/Home';
import WallpaperDetails from '../Wallpaper/WallpaperDetails';
import FullWallpaperView from '../Wallpaper/fullWallpaperView';
import MyHomeStack  from './customeRouting';
import { createStackNavigator } from '@react-navigation/stack';

// Font Awesome Icons...
import { FontAwesome5 } from '@expo/vector-icons'
import { useRef } from 'react';

const Tab = createBottomTabNavigator();

const Stack = createStackNavigator();

// Hiding Tab Names...
export default function App() {
    // Animated Tab Indicator...
    const tabOffsetValue = useRef(new Animated.Value(0)).current;
    return (
        <NavigationContainer>
            <Stack.Navigator
                screenOptions={() => ({
                    headerShown: false,
                    gestureEnabled: true,
                })}
            >
                <Stack.Screen
                    name="HomeScreen"
                    component={MyHomeStack}
                />
                <Stack.Screen
                    name="FullWallpaperView"
                    component={FullWallpaperView}
                    hasNavigation={false}
                />
            </Stack.Navigator>
        </NavigationContainer>
    );
}

function getWidth() {
    let width = Dimensions.get("window").width

    // Horizontal Padding = 20...
    width = width - 80

    // Total five Tabs...
    return width / 4
}



function SettingsScreen() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Settings!</Text>
        </View>
    );
}

function NotificationScreen() {
    return (
        <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
            <Text>Notifica!</Text>
        </View>
    );
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'center',
        justifyContent: 'center',
    },
});
