import React, { useEffect, useRef, useState } from "react";
import { View, Easing, Animated, Image, Dimensions, StyleSheet,Text } from 'react-native';


const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
var imageWidth, imageHeight;


const FadeInView = (props) => {
    const fadeAnim = new Animated.Value(0);  // Initial value for opacity: 0

    React.useEffect(() => {
        Animated.timing(
            fadeAnim,
            {
                toValue: 1,
                duration: 4000,
                easing: Easing.bounce,
            }
        ).start();
    }, [fadeAnim])


    return (
        <Animated.View                 // Special animatable View
            style={{
                ...props.style,
                opacity: fadeAnim,         // Bind opacity to animated value
            }}
        >
            {props.children}
        </Animated.View>
    );
}




function CardView(props) {

    const [opacity, setOpacity] = useState(new Animated.Value(0));




    if (props.object.height > props.object.width) {
        (props.object.height / screenHeight) * 60 < 300 ? imageHeight = 400 : imageHeight = (props.object.height / screenHeight) * 60;
    }
    else {
        imageHeight = 300;
    }

    return (
        <View>
            <Image
                style={[styles.cardImage, { width: screenWidth, height: imageHeight }]}
                source={{ uri: props.imageUrl }}
            />
            <View style={{ position: 'absolute', bottom: 10, left: 10,flexDirection: 'row',backgroundColor: "rgba(51, 51, 51, 0.3)",  borderRadius: 10,}}>
                <Image
                    style={[{ width: 30, height: 30}]}
                    source={{ uri: props.object.user.profile_image.medium }}
                />
                <View style={{marginLeft:10,marginTop:5,marginRight:10}}>
                <Text style={{color:'white'}}>{(props.object.user.first_name !=null?props.object.user.first_name:" ")+' '+(props.object.user.last_name!=null ? props.object.user.last_name : "")}</Text>
                </View>
                
            </View>

        </View>
    )
}

const styles = StyleSheet.create({

    card: {
        backgroundColor: '#fff',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: '#fff',
        padding: 10,
        shadowColor: '#fff',
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.25,
        shadowRadius: 3.84,
        elevation: 5,
    },
    cardImage: {
        resizeMode: 'cover',
        borderRadius: 12,
        padding: 10,
        marginRight: 10,
        borderWidth: 2,
        borderColor: '#fff',
    },
})

export default CardView;