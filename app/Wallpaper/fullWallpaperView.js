import * as React from 'react';
import {
    Animated,
    View,
    StyleSheet,
    Image,
    Dimensions,
    TouchableOpacity,
    ActivityIndicator,
    Text, TouchableWithoutFeedback,
    AsyncStorage,
    CameraRoll,
} from 'react-native';
import Constants from 'expo-constants';
// import CameraRoll, { save } from "@react-native-community/cameraroll";
import { Permissions, FileSystem } from 'expo';
import { Ionicons,FontAwesome5 } from '@expo/vector-icons';

// You can import from local files
const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const { height, width } = Dimensions.get('window');
const imageSize = 80;
const spaceing = 10;


export default function FullWallpaperView({ route, navigation }) {
    const [images, setImages] = React.useState(null);
    const [scale, setScale] = React.useState(new Animated.Value(1));
    const { imageData } = route.params;
    const translation = React.useRef(new Animated.Value(0)).current;
    React.useEffect(() => {
        
    }, []);

    let scaleTransform = {
        transform: [{ scale: scale }]
    };

    let actionBarY = scale.interpolate({
        inputRange: [0.9, 1],
        outputRange: [0, -80],
    });

    let borderRadiusFunc = scale.interpolate({
        inputRange: [0.9, 1],
        outputRange: [20, 0],
    });

    let opacityFunc= translation.interpolate({
        inputRange: [0, 100],
        outputRange: [0, 1],
    })

    


    const topRef = React.useRef();
    const thumbRef = React.useRef();

    const saveToCameraRoll = async image => {
        const { cameraPermission } = await Permissions.askAsync(Permissions.CAMERA);
        console.log(cameraPermission);
        // let {cameraPermissions} = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        // if (cameraPermissions.status !== 'granted') {
        //     cameraPermissions = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        // }

        // if (cameraPermissions.status === 'granted') {
        //     FileSystem.downloadAsync(
        //         image.urls.regular,
        //         FileSystem.documentDirectory + image.id + '.jpg'
        //     )
        //         .then(({ uri }) => {
        //             CameraRoll.saveToCameraRoll(uri);
        //             alert('Saved to photos');
        //         })
        //         .catch(error => {
        //             console.log(error);
        //         });
        // } else {
        //     alert('Requires cameral roll permission');
        // }
    };

    const showControls = item => {

        Animated.spring(scale, {
            toValue: 0.9,
            useNativeDriver: true,
        }).start();

        Animated.timing(translation, {
            toValue: 100,
            useNativeDriver: true,
        }).start();
    };

    const scrollToActiveIndex = (index) => {
        setActiveIndex(index);
        topRef?.current?.scrollToOffset({
            offset: index * screenWidth,
            animated: true,
        });
        if (index * (imageSize + spaceing) - imageSize / 2 > screenWidth / 2) {

            thumbRef?.current?.scrollToOffset({
                offset:
                    index * (imageSize + spaceing) - screenWidth / 2 + imageSize / 2,
                animated: true,
            });
        } else {
            thumbRef?.current?.scrollToOffset({
                offset: 0,
                animated: true,
            });
        }
    };

    return (
        <View style={styles.container}>
            <View
                style={{
                    position: 'absolute',
                    top: 0,
                    left: 0,
                    right: 0,
                    bottom: 0,
                    backgroundColor: 'black',
                    alignItems: 'center',
                    justifyContent: 'center',
                   
                }}
            >
                <ActivityIndicator size="large" color="grey" />
            </View>
            <TouchableWithoutFeedback onPress={() => showControls(imageData)}>
                <Animated.View style={[{ flex: 1 }, scaleTransform]}>
                    <Animated.Image
                        style={{
                            flex: 1,
                            height: null,
                            width: null,
                            borderRadius: borderRadiusFunc
                        }}
                        source={{ uri: imageData.urls.regular }}
                    />
                </Animated.View>
            </TouchableWithoutFeedback>

            <Animated.View
                style={{
                    position: 'absolute',
                    left: 0,
                    right: 0,
                    bottom: 0,
                    transform: [{ translateY: actionBarY }],
                    opacity: 0,
                    height: 80,
                    backgroundColor: 'black',
                    flexDirection: 'row',
                    justifyContent: 'space-around',
                    opacity: opacityFunc,

                }}
            >
                <View
                    style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                >
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => console.warn("ReLoad")}
                    >
                        <Ionicons name="ios-refresh" color="white" size={40} />
                    </TouchableOpacity>
                </View>
                <View
                    style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                >
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => console.warn("Share")}
                    >
                        <FontAwesome5 name="paint-roller" color="white" size={30} />
                    </TouchableOpacity>
                </View>
                <View
                    style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}
                >
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => saveToCameraRoll(imageData)}
                    >
                        <Ionicons name="ios-download" color="white" size={40} />
                    </TouchableOpacity>
                </View>
            </Animated.View>
        </View>
        // <ImageBackground
        //     source={{
        //         uri: imageData.urls.regular,
        //     }}
        //     style={[{flex:1}]}
        //     resizeMethod="scale"
        //     resizeMode="cover"
        // >
        //    {/* <Text style={styles.text}>Inside</Text> */}
        //   </ImageBackground>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    imageStyle: {
        resizeMode: 'cover',
        // borderRadius: 12,
        // padding: 10,
        // marginRight: 10,
        // borderWidth: 2,
        // borderColor: '#fff',
    },
    text: {
        color: "white",
        fontSize: 42,
        lineHeight: 84,
        fontWeight: "bold",
        textAlign: "center",
        backgroundColor: "#000000c0"
    },
});
