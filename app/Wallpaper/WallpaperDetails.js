import * as React from 'react';
import {
  Text,
  View,
  StyleSheet,
  FlatList,
  Image,
  Dimensions,
  TouchableOpacity,
} from 'react-native';
import Constants from 'expo-constants';

// You can import from local files

// or any pure javascript modules available in npm

import API_KEY from '../Config/index';

const API_URL =
  'https://pixabay.com/api/?key='+API_KEY+'&q=bali&image_type=photo&orientation=vertical&page=1&per_page=20';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const imageSize = 80;
const spaceing = 10;

const fetchImagesFromPexels = async () => {
  const data = await Promise.race([
    fetch(API_URL),
    new Promise((_, reject) =>
      setTimeout(() => reject(new Error('Timeout')), 60000)
    ),
  ]);

  const { hits } = await data.json();
  return hits;
};

export default function WallpaperDetails() {
  const [images, setImages] = React.useState(null);
  const [activeIndex, setActiveIndex] = React.useState(0);

  React.useEffect(() => {
    const fetchImages = async () => {
      const images = await fetchImagesFromPexels();

      setImages(images);
    };

    fetchImages();
  }, []);

  const topRef = React.useRef();
  const thumbRef = React.useRef();

  const scrollToActiveIndex = (index) => {
    setActiveIndex(index);
    topRef?.current?.scrollToOffset({
      offset: index * screenWidth,
      animated: true,
    });
    if (index * (imageSize + spaceing) - imageSize / 2 > screenWidth / 2) {
      
      thumbRef?.current?.scrollToOffset({
        offset:
          index * (imageSize + spaceing) - screenWidth / 2 + imageSize / 2,
        animated: true,
      });
    } else {
      thumbRef?.current?.scrollToOffset({
        offset: 0,
        animated: true,
      });
    }
  };

  if (!images) {
    return (
      <View style={styles.container}>
        <Text>Loading.....</Text>
      </View>
    );
  } else {
    return (
      <View style={styles.container}>
        <FlatList
          ref={topRef}
          data={images}
          keyExtractor={(item) => item.id.toString()}
          horizontal
          pagingEnabled
          showsHorizontalScrollIndicator={false}
         onMomentumScrollEnd ={(ev) => {
            setActiveIndex(
              Math.floor(ev.nativeEvent.contentOffset.x / screenWidth)
            );
          }}
          renderItem={({ item }) => {
            return (
              <View>
                <Image
                  source={{
                    uri: item.largeImageURL,
                  }}
                  style={[{ width: screenWidth, height: screenHeight }]}
                />
              </View>
            );
          }}
        />

        <FlatList
          ref={thumbRef}
          data={images}
          keyExtractor={(item) => item.id.toString()}
          horizontal
          showsHorizontalScrollIndicator={false}
          style={{ position: 'absolute', bottom: 150 }}
          contentContainerStyle={{ paddingHorizontal: spaceing }}
          onMomentumScrollEnd={(ev) => {
            setActiveIndex(
              Math.floor(ev.nativeEvent.contentOffset.x / screenWidth)
            );
          }}
          renderItem={({ item, index }) => {
            return (
              <TouchableOpacity onPress={() => scrollToActiveIndex(index)}>
                <Image
                  source={{
                    uri: item.previewURL,
                  }}
                  style={[
                    {
                      width: imageSize,
                      height: imageSize,
                      borderRadius: 12,
                      marginRight: spaceing,
                      borderWidth: 2,
                      borderColor:
                        activeIndex == index ? '#fff' : 'transparent',
                    },
                  ]}
                />
              </TouchableOpacity>
            );
          }}
        />
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: Constants.statusBarHeight,
    backgroundColor: '#ecf0f1',
  },
});
