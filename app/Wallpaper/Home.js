import React, { useEffect, useState } from "react";
import { View, Text, Image, SafeAreaView, StatusBar, StyleSheet, Dimensions, Easing, Animated } from 'react-native';
import Functions from "../API/Functions";
import Icon from 'react-native-vector-icons/FontAwesome';
import CardView from "../Component/Card/CardView";
import { FlatList, TouchableOpacity } from "react-native-gesture-handler";
import Logo from "../../assets/logo.png";


const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
export default function Home({ navigation }) {
    const [count, setCount] = useState(1);
    const [imageData, setImageData] = useState("");
    const [opacity, setOpacity] = useState(new Animated.Value(0));

    useEffect(() => {
        const fetchUnsplashImage = async () => {
            Functions.getUnsplashImages("photos", "1")
                .then(response => {
                    console.log("Unsplash", response)
                    setImageData(response);
                })
                .catch(err=>console.log("Error in Api:",err))

        };
        //fetchImages();
        fetchUnsplashImage();
    }, []);


    

    useEffect(() => {
        const logoAnimated = () => {
            Animated.timing(opacity, {
                toValue: 1,
                duration: 2000,
                easing: Easing.bounce,
                useNativeDriver: false
            }).start();
        }
        setTimeout(() => {
            logoAnimated();
        }, 1000);
    }, [opacity]);

    const fetchUnsplashImageNextPage = async (pageNumber) => {
        console.log("Page Number", pageNumber);
        Functions.getUnsplashImages("photos", pageNumber)
            .then(response => {
                let newImageData=imageData;
                newImageData=newImageData.concat(response);
                setImageData(newImageData);
                console.log("Unsplash 2", newImageData);
            })

    };

    useEffect(() => {
        count != 0 && count != 1 ? fetchUnsplashImageNextPage(count) : console.log("No More Images");

    }, [count]);

    const displayOnScrollEnd = () => {
        console.log("End");
        if (count < 10 && count > 0) {
            setCount(count+1);
        }
        else {
            setCount(0);
        }
    }


    const animate = () => {
        setOpacity(0);
        Animated.timing(opacity, {
            toValue: 1,
            duration: 2000,
            easing: Easing.bounce,
            useNativeDriver: false
        }).start();
    };

    const size = opacity.interpolate({
        inputRange: [0, 1],
        outputRange: [0, screenWidth]
    });

    const animatedStyles = [
        {
            opacity,
            width: size,
            height: 50
        }
    ];


    const onMomentScrollEndFlatList = (ev) => {
        console.warn("EV: ", Math.floor(ev.nativeEvent.contentOffset.y / screenHeight));
        let enableNextPage = Math.floor(ev.nativeEvent.contentOffset.y / screenHeight);
        if (enableNextPage == 10) {

            fetchUnsplashImageNextPage("2");
        }
    }



    console.log("Count ", count);
   
    if (!imageData) {
        return (
            <View style={styles.container}>
                <Image
                    style={[{ width: 100, height: 50, marginTop: 10, alignSelf: 'center' }]}
                    source={require('../../assets/logo.png')}
                />
                <Text style={{ justifyContent: 'center' }}>Loading.....</Text>
            </View>
        );
    } else {
        return (
            <SafeAreaView style={styles.container}>
                <Animated.View style={animatedStyles}>
                    <Image
                        style={[{ width: 100, height: 50, marginTop: 10, alignSelf: 'center' }]}
                        source={require('../../assets/logo.png')}
                    />
                </Animated.View>


                <Text style={{ marginBottom: 10, fontSize: 16, marginTop: 10, fontWeight: 'bold', marginLeft: 20 }}>MOST POPULAR</Text>
                <FlatList
                    data={imageData}
                    keyExtractor={(item,index) => index.toString()}
                    contentContainerStyle={{ paddingVertical: 10 }}
                    // onMomentumScrollEnd={(ev) => {
                    //     onMomentScrollEndFlatList(ev);
                    // }}
                    onEndReachedThreshold={1}
                    onEndReached={() => {
                        displayOnScrollEnd();
                    }}

                    renderItem={({ item }) => {
                        return (
                            <TouchableOpacity style={{ width: screenWidth, marginBottom: 10 }} onPress={()=>navigation.navigate('FullWallpaperView',{
                                otherParam: 'anything you want here',
                                imageData: item,
                            })}>
                                <CardView imageUrl={item.urls.regular} text={"Unsplash"} object={item} />
                            </TouchableOpacity>
                        )
                    }}
                />

            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: StatusBar.currentHeight,
        // justifyContent: "center",
        alignItems: "flex-start",
        backgroundColor: "white"
    }
})