import { API_KEY, Access_key } from '../Config/index';

const API_URL = 'https://api.unsplash.com/';

const genarateUrl = (query, page) => {
    return 'https://pixabay.com/api/?key=' + API_KEY + '&q=' + query + '&image_type=photo&orientation=vertical&page=' + page + '&per_page=20';
}

const apiHeader = () => {
    let apiHeader = {
        Authorization: "Client-ID " + Access_key,
        'X-Per-Page': 20
    }

    return apiHeader;
}


const genarateUnsplashUrl = (query, page) => {
    let url;
    if (typeof(page) == "undefined") {
        url = API_URL + query;
    }
    else {
        url = API_URL + query + '?page=' + page+'&per_page=20';
    }

    return url;

}

// const fetchImagesFromPixabay = async (url) => {
//     console.log(url);
//     const data = await Promise.race([
//         fetch(url),
//         new Promise((_, reject) =>
//             setTimeout(() => reject(new Error('Timeout')), 60000)
//         ),
//     ]);

//     const {hits} = await data.json();
//     return hits;
// }
const Functions = {

    async getImages(search, page) {

        try {

            let url = genarateUrl(search, page);

            const data = await Promise.race([
                fetch(url),
                new Promise((_, reject) =>
                    setTimeout(() => reject(new Error('Timeout')), 60000)
                ),
            ]);

            const { hits } = await data.json();
            return hits;

        } catch (error) {
            console.log("Function Got Error", error);
        }
    },

    async getUnsplashImages(search, page) {
        try {
            let url = genarateUnsplashUrl(search,page);
            const data = await Promise.race([
                fetch(url, {
                    method: 'GET',
                    headers: apiHeader(),
                    body: "",
                }),
                new Promise((_, reject) =>
                    setTimeout(() => reject(new Error('Timeout')), 60000)
                ),
            ])

            const response = await data.json();
            return response;
        } catch (error) {
            console.log("Error in getUnsplashImage: ", error);
        }
    }


};


export default Functions;
